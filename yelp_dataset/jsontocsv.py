import json
import csv

# Use raw strings or escape backslashes
json_file_path = r'C:\Users\benze\Documents\Woah I_m in College now_ (Not including DE classes)\Fall 2023\CMDA 4634\yelp_dataset\yelp_academic_dataset_checkin.json'
csv_file_path = r'C:\Users\benze\Documents\Woah I_m in College now_ (Not including DE classes)\Fall 2023\CMDA 4634\yelp_dataset\yelp_checkin.csv'

# Specify the encoding when opening the CSV file
with open(json_file_path, 'r', encoding='utf-8') as json_file:
    jsondata = json_file.read().split('\n')  # Split the file by lines

with open(csv_file_path, 'w', newline='', encoding='utf-8') as data_file:
    csv_writer = csv.writer(data_file)

    # Write the header row to the CSV file
    header_written = False

    for line in jsondata:
        try:
            item = json.loads(line)
        except json.JSONDecodeError:
            # Skip this line if there's a JSON parsing error
            continue

        # If the JSON object is successfully parsed, write it to the CSV file
        if not header_written:
            # Write header row on the first valid object
            csv_writer.writerow(item.keys())
            header_written = True

        csv_writer.writerow(item.values())
